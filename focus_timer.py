import time

minutes = int(100)

print()
print('===================================')
print(F'get ready to focus for {minutes} minutes!')
print('===================================')
print()

for i in range(minutes):
	time_remaining = minutes - i
	if time_remaining == 1:
		print(str(minutes - i) + " minute remaining")
		time.sleep(60)
		break
	print(str(minutes - i) + " minutes remaining")
	time.sleep(60)

print()
print('=================')
print('time for a break!')
print('=================')
print()
